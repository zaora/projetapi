<?php
/**
 * Created by PhpStorm.
 * User: Famas
 * Date: 18/01/2019
 * Time: 12:06
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ControllerCategories
{
    function getAllCategories(){
        $Categories = $this->listeCategories();

        $jsonResponse = json_encode($Categories);

        return new Response($jsonResponse);
    }

    function getCategorie($Name = Null){
        $i = 0;
        $IsFound = 0;

        $AllCategories = $this->listeCategories();

        if ($Name == Null || $Name == ''){
            $response = ["error" => "Bad Request"];
            $responseJson = json_encode($response);
            return new Response($responseJson, 400);
        }

        while ($IsFound == 0 && $i < sizeof($AllCategories)){
            if($AllCategories[$i]["Nom"] == $Name){
                $IsFound = 1;
                $localisation = $i;
            }
            $i++;
        }

        if ($IsFound == 0){
            $response = ["error" => "Not found"];
            $responseJson = json_encode($response);
            return new Response($responseJson, 404);
        } else {
            $jsonResponse = json_encode($AllCategories[$localisation]);

            return new Response($jsonResponse);
        }

    }

    function listeCategories(){
        $liste = [
            ["Nom" => "Nom1", "ID" => "1"],
        ["Nom" => "Nom2", "ID" => "2"],
        ["Nom" => "Nom3", "ID" => "3"],
        ["Nom" => "Nom4", "ID" => "4"],
        ["Nom" => "Nom5", "ID" => "5"],
        ["Nom" => "Nom6", "ID" => "6"]
        ];
        return $liste;
    }

}
