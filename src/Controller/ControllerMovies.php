<?php
/**
 * Created by PhpStorm.
 * User: Famas
 * Date: 18/01/2019
 * Time: 10:16
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ControllerMovies
{
    function getAllMovies(){
        $Allmovies = $this->ReturnMovies();

        $jsonResponse = json_encode($Allmovies);

        return new Response($jsonResponse);

    }

    function getMovie($Name = Null){
        $i = 0;
        $IsFound = 0;

        $Allmovies = $this->ReturnMovies();

        if ($Name == Null || $Name == ''){
            $response = ["error" => "Bad Request"];
            $responseJson = json_encode($response);
            return new Response($responseJson, 400);
        }

        while ($IsFound == 0 && $i < sizeof($Allmovies)){
            if($Allmovies[$i]["Titre"] == $Name){
                $IsFound = 1;
                $localisation = $i;
            }
            $i++;
        }

        if ($IsFound == 0){
            $response = ["error" => "Not found"];
            $responseJson = json_encode($response);
            return new Response($responseJson, 404);
        } else {
            $jsonResponse = json_encode($Allmovies[$localisation]);

            return new Response($jsonResponse);
        }
    }


    function ReturnMovies(){
        $Movies = [
            ["Titre" => "Titre 1", "id" => "1","Annee" => "2010","Poster" => "https://nationalinterest.org/sites/default/files/styles/desktop__1486_x_614/public/main_images/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis" => "This is a synopsis", "Actors" => [1,2,4], "Categorie" => 3],
            ["Titre" => "Titre 2", "id" => "2","Annee" => "2010","Poster" => "https://nationalinterest.org/sites/default/files/styles/desktop__1486_x_614/public/main_images/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis" => "This is a synopsis" ,"Actors" => [1,2,4], "Categorie" => 6],
            ["Titre" => "Titre 3", "id" => "3","Annee" => "2010","Poster" => "https://nationalinterest.org/sites/default/files/styles/desktop__1486_x_614/public/main_images/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis" => "This is a synopsis" ,"Actors" => [1,3], "Categorie" => 1],
            ["Titre" => "Titre 4", "id" => "4","Annee" => "2010","Poster" => "https://nationalinterest.org/sites/default/files/styles/desktop__1486_x_614/public/main_images/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis" => "This is a synopsis" ,"Actors" => [2,4], "Categorie" => 2],
            ["Titre" => "Titre 5", "id" => "5","Annee" => "2010","Poster" => "https://nationalinterest.org/sites/default/files/styles/desktop__1486_x_614/public/main_images/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis" => "This is a synopsis" ,"Actors" => [1,4], "Categorie" => 3],
            ["Titre" => "Titre 6", "id" => "6","Annee" => "2010","Poster" => "https://nationalinterest.org/sites/default/files/styles/desktop__1486_x_614/public/main_images/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis" => "This is a synopsis" ,"Actors" => [1,2,4], "Categorie" => 1],
            ["Titre" => "Titre 7", "id" => "7","Annee" => "2010","Poster" => "https://nationalinterest.org/sites/default/files/styles/desktop__1486_x_614/public/main_images/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis" => "This is a synopsis" ,"Actors" => [1,2,3], "Categorie" => 5],
            ["Titre" => "Titre 8", "id" => "8","Annee" => "2010","Poster" => "https://nationalinterest.org/sites/default/files/styles/desktop__1486_x_614/public/main_images/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis" => "This is a synopsis" ,"Actors" => [1,2,4], "Categorie" => 1]
        ];

        return $Movies;
    }

    function addMovie(Request $data){
        $content = $data->getContent();
        $content = json_decode($content);

        $Movies = $this->ReturnMovies();
        array_push($Movies,$content);


        $jsonResponse = json_encode($Movies);
        return new Response($jsonResponse,200);
    }

    function deleteMovie($id){
        $i = 0;
        $IsFound = 0;

        $Allmovies = $this->ReturnMovies();

        while ($IsFound == 0 && $i < sizeof($Allmovies)){
            if($Allmovies[$i]["id"] == $id){
                $IsFound = 1;
                $localisation = $i;
            }
            $i++;
        }

        if ($IsFound == 0){
            $response = ["error" => "Not found"];
            $responseJson = json_encode($response);
            return new Response($responseJson, 404);
        } else {
            unset($Allmovies[$localisation]);
            $Allmovies = array_values($Allmovies);
            $jsonResponse = json_encode($Allmovies);
            return new Response($jsonResponse,200);
        }

    }
}
