<?php
/**
 * Created by PhpStorm.
 * User: Famas
 * Date: 18/01/2019
 * Time: 10:05
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class login
{
    function login($input){
        $content = $input->getContent();
        $data = json_decode($content);
        if (false === isset($data->username, $data->password)) {
            $response = ["error" => "Missing arguments"];
            $responseJson = json_encode($response);
            return new Response($responseJson, 400);
        }
        if ($data->username === 'user' && $data->password === 'azerty') {
            $token = ["token" => "12O3KROFK4O45TKF"];
            $jsonToken = json_encode($token);
            return new Response($jsonToken);
        } else {
            $response = ["error" => "Bad Credentials"];
            $responseJson = json_encode($response);
            return new Response($responseJson, 400);
        }
    }
}
