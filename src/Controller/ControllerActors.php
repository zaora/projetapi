<?php
/**
 * Created by PhpStorm.
 * User: Famas
 * Date: 18/01/2019
 * Time: 12:36
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ControllerActors
{
    function getAllActors(){
        $Actors = $this->ReturnActors();

        $jsonResponse = json_encode($Actors);

        return new Response($jsonResponse);
    }

    function getActor($Name = Null){
        $i = 0;
        $IsFound = 0;

        $Actors = $this->ReturnActors();

        if ($Name == Null || $Name == ''){
            $response = ["error" => "Bad Request"];
            $responseJson = json_encode($response);
            return new Response($responseJson, 400);
        }

        while ($IsFound == 0 && $i < sizeof($Actors)){
            if($Actors[$i]["Name"] == $Name){
                $IsFound = 1;
                $localisation = $i;
            }
            $i++;
        }

        if ($IsFound == 0){
            $response = ["error" => "Not found"];
            $responseJson = json_encode($response);
            return new Response($responseJson, 404);
        } else {
            $jsonResponse = json_encode($Actors[$localisation]);

            return new Response($jsonResponse);
        }
    }

    function ReturnActors(){
        $Actors = [
        ["Name" => "John", "Surname" => "Doe", "ID" => 1,"Movies_id" => [1,2,3]],
        ["Name" => "Helena", "Surname" => "Brooklyn", "ID" => 2,"Movies_id" => [1,2,3]],
        ["Name" => "Prinz", "Surname" => "Eugen", "ID" => 3,"Movies_id" => [1,2,3]],
        ["Name" => "Missouri", "Surname" => "Iowa", "ID" => 4,"Movies_id" => [1,2,3]]
        ];
        return $Actors;
    }

    function deleteActor($id){
        $i = 0;
        $IsFound = 0;

        $AllActors = $this->ReturnActors();

        while ($IsFound == 0 && $i < sizeof($AllActors)){
            if($AllActors[$i]["ID"] == $id){
                $IsFound = 1;
                $localisation = $i;
            }
            $i++;
        }

        if ($IsFound == 0){
            $response = ["error" => "Not found"];
            $responseJson = json_encode($response);
            return new Response($responseJson, 404);
        } else {
            unset($AllActors[$localisation]);
            $AllActors = array_values($AllActors);
            $jsonResponse = json_encode($AllActors);
            return new Response($jsonResponse,200);
        }

    }

    function addActor(Request $request){
        $content = $request->getContent();
        $content = json_decode($content);

        $Actors = $this->ReturnActors();
        array_push($Actors,$content);


        $jsonResponse = json_encode($Actors);
        return new Response($jsonResponse,200);
    }

    function editActor(Request $request){
        $content = $request->getContent();

        $jsonResponse = json_encode($content);

        return new Response($jsonResponse,200);
    }
}
