# API Doc

## Movies

| URI        | Method           | Params  | Response |
| :--------: |:-------------:| :-------- |:---- |
| /movies   | GET |  | 200: ``` [{"Titre":"Titre 1","id":"1","Annee":"2010","Poster":"https:\/\/nationalinterest.org\/sites\/default\/files\/styles\/desktop__1486_x_614\/public\/main_images\/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis":"This is a synopsis","Actors":[1,2,4],"Categorie":3}, {}] ``` |
| /movie   | POST      |   *required* body ``` {"Titre":"Titre 1","Annee":"2010","Poster":"https:\/\/nationalinterest.org\/sites\/default\/files\/styles\/desktop__1486_x_614\/public\/main_images\/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis":"This is a synopsis","Actors":[1,2,4],"Categorie":3} ``` | 201: ``` {"Titre":"Titre 1","id":"1","Annee":"2010","Poster":"https:\/\/nationalinterest.org\/sites\/default\/files\/styles\/desktop__1486_x_614\/public\/main_images\/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis":"This is a synopsis","Actors":[1,2,4],"Categorie":3} ``` |
| /movie/{Name}   | GET      |     | 200: ``` {"Titre":"Titre 1","id":"1","Annee":"2010","Poster":"https:\/\/nationalinterest.org\/sites\/default\/files\/styles\/desktop__1486_x_614\/public\/main_images\/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis":"This is a synopsis","Actors":[1,2,4],"Categorie":3} ``` |
| /movie/{id}   | PUT      |  *required* body ``` {"Titre":"Titre 1","Annee":"2010","Poster":"https:\/\/nationalinterest.org\/sites\/default\/files\/styles\/desktop__1486_x_614\/public\/main_images\/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis":"This is a synopsis","Actors":[1,2,4],"Categorie":3} ```   | 200: ``` {"Titre":"Titre 1","id":"1","Annee":"2010","Poster":"https:\/\/nationalinterest.org\/sites\/default\/files\/styles\/desktop__1486_x_614\/public\/main_images\/uss_new_jersey_6219214852_0.jpg?itok=16I84AdM","Synopsis":"This is a synopsis","Actors":[1,2,4],"Categorie":3}``` |
| /movie/{id}   | DELETE      |     | 200 ``` {} ``` |

## Acteurs

| URI        | Method           | Params  | Response |
| ---------- |:-------------:| -----:| --- |
| /actors   | GET |  | 200: ``` [{"Name":"John","Surname":"Doe","ID":1}, {..} ] ``` |
| /actor   | POST      |   *required* body ``` {"Name":"John","Surname":"Doe","ID":1} ``` | 201: ``` {"Name":"John","Surname":"Doe","ID":1}``` |
| /actor/{Name}   | GET      |     | 200: ```  {"Name":"John","Surname":"Doe","ID":1}     ``` |
| /actor   | PUT      |  *required* body ``` {"title": "Titre", "description": "description", "author": 42, "category": 1  } ``` | 200: ``` { "id": 42, "title": "Titre", "description": "Description de l'article...", "author": 42 }```|
| /actor/{id}   | DELETE   |     | 200 ``` {} ``` |


## Categories

| URI        | Method           | Params  | Response |
| ---------- |:-------------:| -----:| --- |
| /categories   | GET |  | 200: ``` [ {"Nom":"Nom1","ID":"1"},, {..} ] ``` |
| /categorie   | POST      |   *required* body ``` {"Nom":"Nom"} ``` | 201: ``` {"Nom":"Nom1","ID":"1"}``` |
| /categorie/{Name}   | GET      |     | 200: ``` {"Nom":"Nom1","ID":"1"}``` |
| /categorie   | PUT      |  *required* body ``` {"Nom":"Nom"} ```   | 200: ``` {"Nom":"Nom1","ID":"1"}``` |
| /categorie/{id}   | DELETE      |     | 200 ``` {} ``` |
